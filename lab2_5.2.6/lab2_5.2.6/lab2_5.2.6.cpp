﻿#include <iostream>
#include <iomanip>

/*
Описать структуру «время» (часы, минуты, секунды).
Определить функцию «прошедшее время» определяющую интервал времени 
между t1 и t2 в минутах (округление производить в меньшую сторону).
*/

// структура "время"
struct Time
{
	int hours;
	int minutes;
	int seconds;
};

// вывод структуры Time в std::cout
std::ostream &operator<<(std::ostream &s, const Time &t)
{
	return s << std::setw(2) << std::setfill('0') << t.hours
			 << ":"
			 << std::setw(2) << std::setfill('0') << t.minutes
			 << ":"
			 << std::setw(2) << std::setfill('0') << t.seconds;
}

// функция возвращает время в секундах
int Second(const Time &t)
{
	return t.hours * 60 * 60 + t.minutes * 60 + t.seconds;
}

// функция "прошедшее время" определяющая интервал времени 
// между t1 и t2 в минутах
int PassedTime(const Time &t1, const Time &t2)
{
//	return abs(Second(t2) - Second(t1)) / 60;
	return (Second(t2) - Second(t1)) / 60;
}

int main()
{
	Time t1 = {2, 6, 15};
	Time t2 = {18, 0, 0};
	std::cout << "t1 = " << t1 << std::endl;
	std::cout << "t2 = " << t2 << std::endl;
	std::cout << "passed time: " << PassedTime(t1, t2) << " minutes" << std::endl;

	std::cout << std::endl;

	t1 = {20, 6, 15};
	t2 = {12, 0, 0};
	std::cout << "t1 = " << t1 << std::endl;
	std::cout << "t2 = " << t2 << std::endl;
	std::cout << "passed time: " << PassedTime(t1, t2) << " minutes" << std::endl;

	return 0;
}
