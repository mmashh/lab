﻿#include <iostream>
#include <limits>

/*
 Описать структуру с именем STUDENT, содержащую поля:
-	фамилия и инициалы;
-	номер группы;
-	успеваемость (массив из пяти элементов).
Написать программу, выполняющую следующие действия:
-	ввод с клавиатуры данных в массив, состоящий из 5 структур типа STUDENT;
-	вывод на дисплей фамилий и номеров групп для всех студентов, включенных в массив,
		если средний балл студента больше 4.0;
-	если таких студентов нет, вывести соответствующее сообщение.
 */

// количество студентов
const size_t StudentCount = 5;
// макс размер строки "фамилия и инициалы", включая '\0'
const size_t NameMaxSize = 80;
// количество оценок студента (успеваемость, массив из пяти элементов)
const size_t MarkCount = 5;

// структура STUDENT
struct STUDENT
{
	// фамилия и инициалы
	char Name[NameMaxSize];
	// номер группы
	int Group;
	// успеваемость (массив из пяти элементов)
	int Marks[MarkCount];
};

// функция ввода числа из консоли
int InputInt()
{
	int value;
	// бесконечный цикл, пока не введем число
	for (;;)
	{
		// попытка прочитать число из консоли
		std::cin >> value;
		// проверка на ошибку
		if (std::cin.fail())
		{
			std::cout << "<ошибка ввода>" << std::endl;
			// очистка std::cin от признака ошибки
			std::cin.clear();
			// пропуск всех символов до конца строки
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		else // число ok
		{
			// пропуск всех символов до конца строки
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			return value;
		}
	}
}

int main()
{
	std::setlocale(LC_ALL, "Russian");

	// массив из 5 структур типа STUDENT
	STUDENT students[StudentCount];

	// ввод студентов
	for (size_t i = 0; i != StudentCount; ++i)
	{
		STUDENT &student = students[i];
		std::cout << "Студент N " << i + 1 << std::endl;

		std::cout << "фамилия и инициалы: ";
		std::cin.getline(student.Name, NameMaxSize);

		std::cout << "номер группы: ";
		student.Group = InputInt();

		std::cout << "оценки: ";
		for (size_t i = 0; i != MarkCount; ++i)
		{
			std::cout << "оценка" << i + 1 << ": ";
			student.Marks[i] = InputInt();
		}
	}

	//вывод на дисплей фамилий и номеров групп для всех студентов, включенных в массив,
	// если средний балл студента больше 4.0;

	bool botanFound = false;

	for (size_t i = 0; i != StudentCount; ++i)
	{
		const STUDENT &student = students[i];

		double averageScore = 0;

		for (size_t i = 0; i != MarkCount; ++i)
			averageScore += student.Marks[i];

		averageScore /= MarkCount;
		if (averageScore > 4.0)
		{
			if (!botanFound)
			{
				botanFound = true;
				std::cout << "студенты, средний балл которых > 4:" << std::endl;
			}
			std::cout << student.Group << "\t" << student.Name << std::endl;
		}
	}

	if (!botanFound)
		std::cout << "нет студентов с средним баллом > 4" << std::endl;

	return 0;
}
